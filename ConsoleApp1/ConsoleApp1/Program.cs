﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> persos = new List<String>();
            persos.Add("Ben Kenobi");
            persos.Add("Luke Skywalker");
            persos.Add("Darth Vader");
            persos.Add("Darth Sidious");
            persos.Add("Han Solo");
            string path_rebels = @"rebels.txt";
            using (StreamWriter file = new StreamWriter(path_rebels))
            {
                foreach (string perso in persos)
                {
                    if (!perso.Contains("Darth"))
                    {
                        file.WriteLine(perso);
                    }
                }
            }
            using (StreamWriter file = new StreamWriter(path_rebels, true))
            {
                file.WriteLine("Leia Organa");
                file.WriteLine("Wedge Antilles");
            }
            string text = File.ReadAllText(path_rebels);
            Console.WriteLine(text);
            string[] lines = File.ReadAllLines(path_rebels);
            foreach (string line in lines)
            {
                Console.WriteLine(line);
            }
            Console.ReadKey();
        }
    }
}
